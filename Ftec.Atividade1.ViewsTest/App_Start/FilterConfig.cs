﻿using System.Web;
using System.Web.Mvc;

namespace Ftec.Atividade1.ViewsTest
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
